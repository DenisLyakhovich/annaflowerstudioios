//
//  StudioDataProvider.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 30.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit

protocol StudioDataProviderDelegate: class {
    func studioDataLoaded()
    func studioDataHasError(error: String)
}

class StudioDataProvider: NSObject {
    var studio: [StudioModel] = []
    weak var delegate: StudioDataProviderDelegate?
    
    init(d: StudioDataProviderDelegate) {
        self.delegate = d
    }
    
    @objc func refresh() {
        studio = []
        loadStudio()
    }
    
    func loadStudio() {
        TestProjectApiService.sharedInstance.studioApiService.studio(limit: 100, skip: studio.count, complection: { (studioReponse, error) in
            
            if let error = error {
                self.delegate?.studioDataHasError(error: error)
                return
            }else if let studioArray:[StudioModel] = studioReponse{
              
                self.studio.append(contentsOf: studioArray)
                self.delegate?.studioDataLoaded()
                print("Load Studio")
                return
                
            }
            self.delegate?.studioDataLoaded()
            
        })
    }
}


