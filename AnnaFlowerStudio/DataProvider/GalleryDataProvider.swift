//
//  GalleryDataProvider.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 02.05.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit

protocol GalleryDataProviderDelegate: class {
    func galleryDataLoaded()
    func galleryDataHasError(error: String)
    
}

class GalleryDataProvider: NSObject {
    
    var  gallery: [GalleryModel] = []
    
    weak var delegate: GalleryDataProviderDelegate?
    
    init(d: GalleryDataProviderDelegate) {
        self.delegate = d
    }
    
    func loadGallery(index: StudioModel) {
        TestProjectApiService.sharedInstance.galleryApiService.gallery(limit: 500, skip: gallery.count, complection: { (galleryReponse, error) in
            
            if let error = error {
                
                self.delegate?.galleryDataHasError(error: error)
                return
            }else if let galleryArray:[GalleryModel] = galleryReponse {
                
                for element in galleryArray {
                    if element.titleId == index.title {
                        self.gallery.append(element)
                    }
                }
                self.delegate?.galleryDataLoaded()
                print("Load comments")
                print("PostID \(String(describing: index.id))")
                return
                
            }
            self.delegate?.galleryDataLoaded()
        })
    }
}



