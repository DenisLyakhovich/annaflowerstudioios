//
//  GalleryTableViewCell.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 25.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import Foundation

class GalleryTableViewCell: UITableViewCell {
    
    static let nibName = "GalleryTableViewCell"
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var flowerLabel: UILabel!
    @IBOutlet weak var flowerImageOne: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        numberLabel.layer.cornerRadius = numberLabel.frame.height / 2.0
        numberLabel.layer.masksToBounds = true
        flowerImageOne.layer.cornerRadius = flowerImageOne.frame.height / 9.0
        flowerImageOne.layer.masksToBounds = true
    }
    
    func  castomize(gallery: GalleryModel, studio: StudioModel) {
        dateLabel.text = gallery.data
        numberLabel.text = gallery.number
        flowerLabel.text = studio.title
        flowerImageOne.image = self.imageParse(gallery: gallery)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func imageParse(gallery: GalleryModel) -> UIImage {
        if let imga = gallery.imga {
            let url = URL(string: imga)
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if  data == nil {
                    print("No image, connecting the network")
                } else {
                    DispatchQueue.main.async {
                        self.flowerImageOne.image = UIImage(data: data!)
                    }
                }
            }).resume()
        }
        return UIImage()
    } 
}












