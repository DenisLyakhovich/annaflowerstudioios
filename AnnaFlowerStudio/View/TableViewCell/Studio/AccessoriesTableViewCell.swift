//
//  FlowerTableViewCell.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 25.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit

class AccessoriesTableViewCell: UITableViewCell {
    
    static let nibName = "AccessoriesTableViewCell"
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var flowerLabel: UILabel!
    @IBOutlet weak var flowerImageOne: UIImageView!
    @IBOutlet weak var flowerImageTwo: UIImageView!
    @IBOutlet weak var flowerImageThree: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func  castomize(post: StudioModel) {
        photoLabel.layer.cornerRadius = photoLabel.frame.height / 2.0
        photoLabel.layer.masksToBounds = true
        flowerLabel.text = post.title
        priceLabel.text = post.price
        photoLabel.text = post.photo
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
