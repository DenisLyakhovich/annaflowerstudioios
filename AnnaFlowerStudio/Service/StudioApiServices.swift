//
//  StudioApiServices.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 30.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias StudioComplection = (_ studio: [StudioModel]?, _ error: String?) -> Void

class StudioApiServices: BaseApiService {
    func  studio (limit: Int, skip: Int, complection: @escaping StudioComplection)  {
        let url = host + "/api/studio"
        
        var params:[String:AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: .get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    complection(nil, error?.localizedDescription)
                    return
                } else if let studio = responseData!.arrayObject {
                    let studioModel = Mapper<StudioModel>().mapArray(JSONObject: studio)
                    complection(studioModel, nil)
                    return
                }
                complection(nil, "Ошибка при загрузке")
        }
    }
}





