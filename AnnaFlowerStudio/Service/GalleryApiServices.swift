//
//  GalleryApiServices.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 02.05.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias GalleryComplection = (_ gallery: [GalleryModel]?, _ error: String?) -> Void

class GalleryApiServices: BaseApiService {
    func  gallery (limit: Int, skip: Int, complection: @escaping GalleryComplection)  {
        let url = host + "/api/gallery"
        
        var params:[String:AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        
        self.sendRequestWithJSONResponse(
            requestType: .get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (responseData, error) in
                
                if error != nil {
                    complection(nil, error?.localizedDescription)
                    return
                } else if let gallery = responseData!.arrayObject {
                    let galleryModel = Mapper<GalleryModel>().mapArray(JSONObject: gallery)
                    complection(galleryModel, nil)
                    return
                }
                complection(nil, "Ошибка при загрузке")
        }
    }
}
