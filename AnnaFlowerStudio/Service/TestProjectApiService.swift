//
//  TestProjectApiService.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 30.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit

class TestProjectApiService: NSObject {
    
    static let sharedInstance: TestProjectApiService = {
        TestProjectApiService()
    }()
    
    private(set) var studioApiService: StudioApiServices
    private(set) var galleryApiService: GalleryApiServices
    
    private override init() {
        self.studioApiService = StudioApiServices()
        self.galleryApiService = GalleryApiServices()
    }
}
