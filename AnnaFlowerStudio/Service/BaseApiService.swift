//
//  BaseApiService.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 30.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import Alamofire
import SwiftyJSON

typealias RequestJsonComplection = (_ results:JSON?, _ error:Error?) -> Void

protocol DefaultRequestResponseDelegate: class {
    func requestDone()
    func requestHas(error: String)
}

class BaseApiService: NSObject {
    
    struct APIManager {
        static let sharedManager: SessionManager = {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 20
            return SessionManager(configuration: configuration)
        }()
    }
    
    internal let host = "http://denvich.com"
    
    internal func sendRequestWithJSONResponse(
        requestType: HTTPMethod,
        url: String,
        params:[String:AnyObject]?,
        headers: HTTPHeaders?,
        paramsEncoding: ParameterEncoding,
        complection:@escaping RequestJsonComplection) {
        
        debugPrint(params as Any)
        
        var reqHeaders = HTTPHeaders()
        if headers != nil {
            reqHeaders = headers!
        }
        
        debugPrint(reqHeaders)
        
        APIManager.sharedManager.request(
            url as URLConvertible,
            method: requestType,
            parameters: params,
            encoding: paramsEncoding,
            headers: reqHeaders).responseData  { (dataResponse) in
                
                debugPrint(dataResponse)
                switch dataResponse.result {
                case .success(let data):
                    let json = JSON(data)
                    print(json)
                    complection(json, nil)
                case .failure(let error):
                    if let response = dataResponse.response,
                        response.statusCode == 201 {
                        complection(nil,nil)
                    } else if (error as NSError).code != -999 {
                        complection(nil, error)
                    }
                }
        }
    }

}

