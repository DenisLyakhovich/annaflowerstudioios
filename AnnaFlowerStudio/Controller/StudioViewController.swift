//
//  StudioViewController.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 25.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import Alamofire

class StudioViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, StudioDataProviderDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var dataProvider: StudioDataProvider? = nil
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: FlowerTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: FlowerTableViewCell.nibName)
        tableView.register(UINib(nibName: BouquetTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: BouquetTableViewCell.nibName)
        tableView.register(UINib(nibName: CompositionTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CompositionTableViewCell.nibName)
        tableView.register(UINib(nibName: AccessoriesTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: AccessoriesTableViewCell.nibName)
        dataProvider = StudioDataProvider(d: self)
        dataProvider?.loadStudio()
        self.refreshData()
     
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func  tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataProvider == nil {
            return 0
        }
            return dataProvider!.studio.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let item = dataProvider!.studio[indexPath.row]
        switch item.id {
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: FlowerTableViewCell.nibName) as! FlowerTableViewCell? {
                cell.castomize(post: dataProvider!.studio[indexPath.row])
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: BouquetTableViewCell.nibName) as! BouquetTableViewCell? {
                cell.castomize(post: dataProvider!.studio[indexPath.row])
                return cell
            }
        case 3:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CompositionTableViewCell.nibName) as! CompositionTableViewCell? {
                cell.castomize(post: dataProvider!.studio[indexPath.row])
                return cell
            }
        case 4:
            if let cell = tableView.dequeueReusableCell(withIdentifier: AccessoriesTableViewCell.nibName) as! AccessoriesTableViewCell? {
                cell.castomize(post: dataProvider!.studio[indexPath.row])
                return cell
            }
        default:
            return UITableViewCell()
        
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 173
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = dataProvider!.studio[indexPath.row]
        switch item.id {
        case 1:
         RouteData.routeArticle(st: dataProvider!.studio[indexPath.row], fromVC: self)
        case 2:
         RouteData.routeArticle(st: dataProvider!.studio[indexPath.row], fromVC: self)
        case 3:
          RouteData.routeArticle(st: dataProvider!.studio[indexPath.row], fromVC: self)
        case 4:
            RouteData.routeArticle(st: dataProvider!.studio[indexPath.row], fromVC: self)
        default: break
        }
    }
    
    func studioDataLoaded() {
        tableView.reloadData()
        refreshControl.endRefreshing()
        print("Reload studio")
    }
    
    func studioDataHasError(error: String) {
        self.alertData()
    }
    
    
    func alertData() {
        if dataProvider!.studio.count == 0 {
            let alert = UIAlertController(title: "Отсутствуют данные...",message: "Подключитесь к сети и обновите вид",preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK",style: .default)  {
                [unowned self] action in
                self.refreshData()
            }
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
    
    func refreshData() {
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(dataProvider, action: #selector(dataProvider?.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        refreshControl.endRefreshing()
    }
}

    /*
    extension UIImageView {
 
        public func imageFromUrl(urlString: String) {
 
            if let url = URL(string: urlString) {
                let request = URLRequest(url: url)
 
                NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: .main, completionHandler: { (response, data, error) in
                    if let imageData = data as NSData? {
                        self.image = UIImage(data: imageData as Data)
 
                    }
                })
 
            }
            print("Sucsess request")
        }
    }
    */


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


