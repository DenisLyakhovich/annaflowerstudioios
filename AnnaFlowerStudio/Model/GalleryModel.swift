//
//  GalleryModel.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 02.05.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import ObjectMapper

class GalleryModel: NSObject, Mappable {
    
    var id: Int?
    var titleId: String?
    var data: String?
    var number: String?
    var imga: String?
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping (map: Map) {
        id                    <- map["id"]
        titleId                <- map["titleId"]
        data                  <- map["data"]
        number                  <- map["number"]
        imga                      <- map["imga"]
    }
}
