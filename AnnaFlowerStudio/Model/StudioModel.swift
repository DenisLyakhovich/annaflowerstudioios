//
//  MainModel.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 30.04.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import ObjectMapper

class StudioModel: NSObject, Mappable{
    
    var id: Int?
    var userId: Int?
    var title: String?
    var price: String?
    var photo: String?
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping (map: Map) {
        id                    <- map["id"]
        userId                <- map["userId"]
        title                 <- map["title"]
        price                 <- map["price"]
        photo                 <- map["photo"]
    }
}

