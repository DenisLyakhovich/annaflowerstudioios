//
//  RouteData.swift
//  AnnaFlowerStudio
//
//  Created by Denis Lyakhovich on 01.05.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import Foundation

class RouteData {
    static func routeArticle (st: StudioModel, fromVC: UIViewController) {
        let toVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
       toVc.st = st
        fromVC.navigationController?.pushViewController(toVc, animated: true)
    }
}
